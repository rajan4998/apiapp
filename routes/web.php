<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/','PostsController@index');
Route::get('/user/login','UserController@index');
Route::get('/user/logout','UserController@logout');
Route::get('/user/import','UserController@import');
Route::get('/posts/create','PostsController@create');

Route::get('/login','UserController@redirect_to_login');
// Route::get('/register','UserController@redirect_to_login');

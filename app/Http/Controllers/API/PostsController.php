<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Posts;
use Auth;

class PostsController extends Controller
{
    //expected parameters
    //logged in user's id
    //from Auth variable
    //returns posts for a
    //particular user
    //on /posts route
    //expects GET request
    public function index(Request $r){
        //getting all posts
        //created by the requested user
        $posts = Posts::where('user_id',Auth::user()->id)->orderBy('created_at','DESC')->get();
        //return json response with
        //all the related posts of
        //a user
        return response()->json(['posts' => $posts]);
    }

    //expected parameters
    //post title, short_description, description
    //and logged in user's id
    //from Auth variable
    //created posts for a
    //particular user
    //on /posts/create route
    //expects POST request
    public function create(Request $r){
        //logged in status verification
        //of requesting user
        if(Auth::check()){
            //initializing blank arrays
            //and variable
            $error = [];
            $post = NULL;

            //getting post title
            //from reqeust variable
            //submitted by the user
            $title = $r->get('title');

            //getting post short_description
            //from reqeust variable
            //submitted by the user
            $short_description = $r->get('short_description');

            //getting post description
            //from reqeust variable
            //submitted by the user
            $description = $r->get('description');

            //checking if title is blank
            //creating error relevant to it
            if($title==""){
                //assigning relevant error in error array
                $error[] = "Title cannot be empty";
            }

            //checking if short_description is blank
            //creating error relevant to it
            if($short_description==""){
                //assigning relevant error in error array
                $error[] = "Short Description cannot be empty";
            }

            //checking if description is blank
            //creating error relevant to it
            if($description==""){
                //assigning relevant error in error array
                $error[] = "Description cannot be empty";
            }

            //checking error array count
            //and returning relevant
            //json response
            if(count($error) > 0){
                //return json response if there's any error in user submission
                return response()->json(['post' => '','code'=>403,'message' => $error]);
            }
            else{
                //if user submitted data passes the
                //blank check validation
                //new post is created
                //against the user_id
                //in the system
                $post = Posts::create([
                    'user_id'=> Auth::user()->id,
                    'title' => $title,
                    'short_description' => $short_description,
                    'description' => $description,
                ]);
                //if post is created successfully
                //return json response accordingly
                if($post!=NULL){
                    //return success json response after
                    //the pose is created successfully
                    return response()->json(['post' => $post,'code'=>200,'message' => 'Post created successfully']);
                }
                else{
                    //return failed json response after
                    //the post is not created
                    return response()->json(['post' => '','code'=>404,'message' => 'failed']);
                }
            }
        }
        else{
            //return failed json response if
            //the requesting user is not logged into system
            return response()->json(['post' => '','code'=>404,'message' => 'User must be login to access this resource']);
        }
    }

    //expected parameters
    //post id,title, short_description, description
    //and logged in user's id
    //from Auth variable
    //updates posts for a
    //particular user
    //on /posts/edit route
    //expects POST request
    public function update(Request $r){
        //logged in status verification
        //of requesting user
        if(Auth::check()){

            //initializing blank arrays
            $error = [];
            //getting post id,title
            //short_description and description
            //from reqeust variable
            //submitted by the user
            $post_id = $r->get('post_id');
            $title = $r->get('title');
            $short_description = $r->get('short_description');
            $description = $r->get('description');

            //checking if title is blank
            //creating error relevant to it
            if($title==""){
                //assigning relevant error in error array
                $error[] = "Title cannot be empty";
            }

            //checking if short_description is blank
            //creating error relevant to it
            if($short_description==""){
                //assigning relevant error in error array
                $error[] = "Short Description cannot be empty";
            }

            //checking if description is blank
            //creating error relevant to it
            if($description==""){
                //assigning relevant error in error array
                $error[] = "Description cannot be empty";
            }

            //checking error array count
            //and returning relevant
            //json response
            if(count($error) > 0){
                //return json response if there's any error in user submission
                return response()->json(['post' => '','code'=>403,'message' => $error]);
            }
            else{
                //getting post by id
                //against the user_id
                //created by the requested user
                $post = Posts::where('id',$post_id)->where('user_id',Auth::user()->id)->first();
                //checking if there's any post exists
                //against user_id and
                //post_id
                if(count($post) > 0){
                    //if there's a post
                    //relevant to requesting user_id
                    //and post_id
                    //updating the post with
                    //the variables provided
                    //in the request variable
                    $post->update([
                        'title' => $title,
                        'short_description' => $short_description,
                        'description' => $description,
                    ]);
                    //return success json response after
                    //the post is updated successfully
                    return response()->json(['post' => $post,'code'=>200,'message' => 'success']);
                }
                else{
                    //return failed json response after
                    //the post is not updated
                    return response()->json(['post' => '','code'=>404,'message' => 'failed']);
                }
            }
        }
        else{
            //return failed json response if
            //the requesting user is not logged into system
            return response()->json(['post' => '','code'=>404,'message' => 'User must be login to access this resource']);
        }
    }

    //expected parameters
    //post id
    //and logged in user's id
    //from Auth variable
    //deletes posts by id for a
    //particular user
    //on /posts/delete route
    //expects POST request
    public function destroy(Request $r){
        //logged in status verification
        //of requesting user
        if(Auth::check()){

            //initializing blank arrays
            //and variables
            $error = [];
            $post = NULL;

            //getting post id
            //from reqeust variable
            //submitted by the user
            $post_id = $r->get('post_id');

            //check if post_id
            //not blank
            if($post_id==""){
                //generating the error accordingly
                $error[] = "Invalid request";
            }

            //checking if there's
            //any error in error Array
            if(count($error) > 0){
                //return json response if there's any error in user submission
                return response()->json(['post' => '','code'=>403,'message' => $error]);
            }
            else{
                //getting post by id
                //against the user_id
                //created by the requested user
                $post = Posts::where('id',$post_id)->where('user_id',Auth::user()->id)->first();

                //checking if there's any post exists
                //against user_id and
                //post_id
                if($post!=NULL){
                    //deleting the particular post
                    $post->delete();
                    //return success json response after
                    //the post is deleted successfully
                    return response()->json(['post' => $post,'code'=>200,'message' => 'success']);
                }
                else{
                    //return failed json response after
                    //the post is not deleted
                    return response()->json(['post' => '','code'=>404,'message' => 'failed']);
                }
            }
        }
        else{
            //return failed json response if
            //the requesting user is not logged into system
            return response()->json(['post' => '','code'=>404,'message' => 'User must be login to access this resource']);
        }
    }

    //expected parameters
    //posts array
    //and logged in user's id
    //from Auth variable
    //creates bulk posts for a
    //particular user
    //on /posts/import route
    //expects POST request
    public function import(Request $r){
        //logged in status verification
        //of requesting user
        if(Auth::check()){

            //initializing blank arrays
            //and variables
            $error = [];
            $posts = [];
            $processed_data = [];
            $created_posts = [];

            //getting file data array
            //from reqeust variable
            //submitted by the user
            $posts = $r->get('file');
            //looping into the data provided by the user
            //and cheking for blank entries and fields
            //to filter out the relevant data
            for ($i=0; $i<count($posts); $i++) {
                // return $posts[$i]['short_description'];
                //check if a certain post
                //has title, short_description
                //and description and are not blank
                if(isset($posts[$i]['title']) && isset($posts[$i]['short_description']) && isset($posts[$i]['description'])){

                    //generating final array for inserting
                    //if it passes through the blank check
                    //validation
                    $processed_data[$i]['user_id'] = Auth::user()->id;
                    $processed_data[$i]['title'] = $posts[$i]['title'];
                    $processed_data[$i]['short_description'] = $posts[$i]['short_description'];
                    $processed_data[$i]['description'] = $posts[$i]['description'];

                    //creating individual posts one by one
                    //from the use submitted data
                    //and assiging created posts array
                    $created_posts[] = Posts::create($processed_data[$i]);
                }
            }

            //check the count for created posts
            //during bulk upload
            if(count($created_posts) > 0){

                //return success json response after
                //the post is deleted successfully
                return response()->json(['post' => $created_posts,'code'=>200,'message' => 'success']);
            }
            else{

                //return failed json response after
                //the post is not deleted
                return response()->json(['post' => '','code'=>404,'message' => 'failed']);
            }
        }
        else{

            //return failed json response if
            //the requesting user is not logged into system
            return response()->json(['post' => '','code'=>404,'message' => 'User must be login to access this resource']);
        }
    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use Auth;

class PassportController extends Controller
{
    //assiging public variable
    //with a value
    public $successStatus = 200;

    /**
    * login api
    *
    * @return \Illuminate\Http\Response
    */
    //expected parameters
    //email, password
    //returns success message and
    //access token for a validated user
    //on /login route
    public function login(){
        //Auth check to check whether the requesting user is a valid user
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            //if user gets logged in
            //user object is passed to a variable
            $user = Auth::user();
            //getting access token for the logged in user
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            //returning success response to request in json form
            return response()->json(['success' => $success,'status' => 200], $this->successStatus);
        }
        else{
            //returning error response to request in json form
            return response()->json(['error'=>'Unauthorised','status' => 403]);
        }
    }

    /**
    * Register api
    *
    * @return \Illuminate\Http\Response
    */
    //expected parameters
    //name, email, password, c_password
    //registers new user in the system if conditions satisfied
    //returns success message and
    //access token for a validated user
    //on /register route
    public function register(Request $request)
    {
        //validating user request with the provided
        //parameters from the request variable
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        //conditional to check validator
        //and corresponding return
        //json response
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        //getting all the data
        //from user request variable
        $input = $request->all();
        //encrypting user submitted password
        //before stroring in the DB
        $input['password'] = bcrypt($input['password']);
        //create user from the data provided
        $user = User::create($input);
        //getting access token for the
        //newly registered user
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        //assiging name to success array
        $success['name'] =  $user->name;

        //success return response in json
        return response()->json(['success'=>$success], $this->successStatus);
    }

    /**
    * details api
    *
    * @return \Illuminate\Http\Response
    */
    public function getDetails()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}

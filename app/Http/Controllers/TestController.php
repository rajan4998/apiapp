<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{



    public function redirect(){
        $query = http_build_query([
            'client_id'     => '3',
            'redirect_uri'  => '/oauth/callback',
            'response_type' => 'code',
            'scope'         => '',
        ]);

        return redirect('/oauth/authorize?' . $query);
    }

    public function callback(){
        $http = new \GuzzleHttp\Client;

        if (request('code')) {
            $response = $http->post('/oauth/token', [
                'form_params' => [
                    'grant_type'    => 'authorization_code',
                    'client_id'     => '3',
                    'client_secret' => 'WymZPfKJIrFUNyNxDXxFI84ZcKuGZvnmuqgngwCq',
                    'redirect_uri'  => '/oauth/callback',
                    'code'          => request('code'),
                ],
            ]);

            return json_decode((string)$response->getBody(), TRUE);
        } else {
            return response()->json(['error' => request('error')]);
        }
    }




    public function index(){
        $client = new GuzzleHttp\Client;

        try {
            $response = $client->post('/oauth/token', [
                'form_params' => [
                    'client_id' => 2,
                    // The secret generated when you ran: php artisan passport:install
                    'client_secret' => 'fx5I3bspHpnuqfHFtvdQuppAzdXC7nJclMi2ESXj',
                    'grant_type' => 'password',
                    'username' => 'rajan4998@gmail.com',
                    'password' => 'secret',
                    'scope' => '*',
                ]
            ]);

            // You'd typically save this payload in the session
            $auth = json_decode( (string) $response->getBody() );
            dd($auth);

            $response = $client->get('http://todos.dev/api/todos', [
                'headers' => [
                    'Authorization' => 'Bearer '.$auth->access_token,
                ]
            ]);

            $todos = json_decode( (string) $response->getBody() );

            $todoList = "";
            foreach ($todos as $todo) {
                $todoList .= "<li>{$todo->task}".($todo->done ? '✅' : '')."</li>";
            }

            echo "<ul>{$todoList}</ul>";

        } catch (GuzzleHttp\Exception\BadResponseException $e) {
            echo "Unable to retrieve access token.";
        }
    }
}

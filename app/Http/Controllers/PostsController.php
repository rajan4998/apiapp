<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PostsController extends Controller
{
    //expects no parameter
    //renders posts index view
    //on /posts/all route
    public function index(){
        return view('posts.index');
    }

    //expects no parameter
    //renders post create view
    //on /posts/create route
    public function create(){
        return view('posts.create');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    //expects no parameter
    //renders user login view
    //on /user/login route
    public function index(){
        $status = 1;
        return view('user.login')->withstatus($status);
    }

    //expects no parameter
    //renders posts import view
    //on /user/import
    public function import(){
        return view('user.import');
    }

    //expects no parameter
    //renders no view
    //redirects to login screeen
    //on /user/logout
    public function logout(){
        Auth::logout();
        return redirect('/user/login');
    }

    public function redirect_to_login(){
        return redirect('/user/login');
    }
}

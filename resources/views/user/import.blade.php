@extends('layouts.master')
@section('content')
<style>
#files,.form-head{
	display: block;
	margin: 0 auto;
}
.validations{
	color: red;
	margin-left: 18px;
}
.uploadBtn-div{
	margin-left: 36px;
}
body{
	background-color: #fff;
}
</style>

<div class="container" ng-app="myApp" ng-controller="myCtrl">
	{{ csrf_field() }}
	<a class="btn btn-default" href="/"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
	<form class="form-horizontal">
		<fieldset>
			<legend>Bulk Post Upload</legend>
			<!-- File Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="file"></label>
				<div class="col-md-4">
					<input id="files" name="file" class="input-file" type="file">
					<span class="file-error validations"></span>
				</div>
			</div>

			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="singlebutton"></label>
				<div class="col-md-4 uploadBtn-div">
					<a ng-click="uploadExcel()" class="btn btn-primary uploadBtn">Submit</a>
					<a href="/Sample-Posts.xlsx" class="btn btn-default">Download Sample</a>

				</div>
			</div>
		</fieldset>
	</form>
</div>
<script src="{{ asset('js/xlsx-0.7.7.core.min.js') }}"></script>
<script>
var result = [];
function handleFile(e){

	//Get the files from Upload control
	var files = e.target.files;
	var i, f;
	//Loop through files
	for (i = 0, f = files[i]; i != files.length; ++i) {
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function (e) {
			var data = e.target.result;

			var result;
			var workbook = XLSX.read(data, { type: 'binary' });

			var sheet_name_list = workbook.SheetNames;
			sheet_name_list.forEach(function (y) { /* iterate through sheets */
				//Convert the cell value to Json
				var row = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
				if (row.length > 0) {
					result = row;
				}
			});
			localStorage.setItem('excel_data',JSON.stringify(result));
		};
		reader.readAsArrayBuffer(f);
	}
}

//Change event to dropdownlist
$("document").ready(function(){
	$('#files').change(handleFile);
});
</script>

<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
	var token = localStorage.getItem('access_token');
	var csrf_token = document.getElementsByName('_token')[0].value;

	$scope.uploadExcel = function(){

		var formatted_array = [];
		var excel_data = localStorage.getItem('excel_data');

		if(excel_data!=null){
			$.each(JSON.parse(excel_data),function(i,value){
				formatted_array[i] = {
					'title' : value.title,
					'short_description' : value.short_description,
					'description' : value.description,
				};
			});
		}

		if(formatted_array.length > 0){
			$http({
				method : "POST",
				url : "/api/posts/import",
				data : {
					file :formatted_array
				},
				headers: {
					'X-CSRF-TOKEN': csrf_token,
					'Authorization' : "Bearer "+token,
					'Accept' : 'application/json'
				}
			})
			.then(function(response) {
				if(response.data.code===200){
					localStorage.removeItem('excel_data');
					window.location = '/';
				}
			});
		}else{
			$('.file-error').html('Select an Excel file');
		}
	}
});

</script>
@endsection

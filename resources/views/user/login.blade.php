@extends('layouts/master')
@section('content')
<style>
.validations{
    color: red;
}
</style>
<div class="container" ng-app="myApp" ng-controller="myCtrl">
    <form class="form-horizontal" id="loginform" name="loginform">
        <fieldset>
            <!-- Form Name -->
            <legend>Login Form</legend>
            {{ csrf_field() }}
            <!-- Text input-->

            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Email</label>
                <div class="col-md-4">
                    <input ng-model="userEmail" id="email" name="email" type="text" placeholder="abc@defmail.com" class="form-control input-md" required="">
                </div>
                <span class="title-error validations"></span>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Password</label>
                <div class="col-md-4">
                    <input ng-model="userPassword" id="password" name="password" type="password" placeholder="" class="form-control input-md" required="">
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="loginBtn"></label>
                <div class="col-md-4">
                    <a id="loginBtn" class="btn btn-primary" ng-click="userLogin()">Login</a>
                    <span>New User ?<a href="/register"> Sign Up</a></span>
                </div>
            </div>
        </fieldset>
    </form>
</div>
<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {

    var token = localStorage.getItem('access_token');
    $scope.load = function() {
        if(token==""){
            window.location = "/user/login";
        }else{
            window.location = "/";
        }
    }

    $scope.userLogin = function(){
        $('.title-error').html('');
        $scope.email = this.userEmail;
        $scope.password = this.userPassword;
        if($scope.email!="" && $scope.password!=""){
            $http({
                method : "POST",
                url : "/api/login",
                handleError:true,
                data : {
                    email :$scope.email,
                    password : $scope.password
                },
            })
            .then(function(response) {
                if(response.data.status===200){
                    localStorage.setItem('access_token',response.data.success.token);
                    window.location = "/";
                }
                if(response.data.status===403){
                    $('.title-error').html('Invalid Credenttials');
                }
            });
        }
    }
});
</script>
@endsection

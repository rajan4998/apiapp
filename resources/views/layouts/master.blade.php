<!DOCTYPE html>
<html>
	<head>
		<title>ApiApp</title>
		<meta charset="utf-8">
		<script src="{{ asset('js/angular-1.6.4.min.js') }}"></script>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="{{ asset('css/bootstrap-3.3.7.min.css') }}">
		<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap-3.3.7.min.js') }}"></script>
	</head>
	<body>
		<div id="app">
	        <nav class="navbar navbar-default navbar-static-top">
	            <div class="container">
	                <div class="navbar-header">

	                    <!-- Collapsed Hamburger -->
	                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
	                        <span class="sr-only">Toggle Navigation</span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                        <span class="icon-bar"></span>
	                    </button>

	                    <!-- Branding Image -->
	                    <a class="navbar-brand" href="{{ url('/') }}">
	                        {{ config('app.name', 'Api App') }}
	                    </a>
	                </div>

	                <div class="collapse navbar-collapse" id="app-navbar-collapse">
	                    <!-- Left Side Of Navbar -->
	                    <ul class="nav navbar-nav">
	                        &nbsp;
	                    </ul>

	                    <!-- Right Side Of Navbar -->
	                    <ul class="nav navbar-nav navbar-right">
	                        <!-- Authentication Links -->
							@if(!isset($status))
								<li><a href="/user/logout" onClick="localStorage.removeItem('access_token')">Logout</a></li>
							@endif
	                    </ul>
	                </div>
	            </div>
	        </nav>
	        @yield('content')
	    </div>
    </body>
</html>

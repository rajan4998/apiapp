@extends('layouts/master')
@section('content')
<style>
.validations{
    color: red;
}
body,#app{
	background-color: #fff;
}
</style>
<div class="container" ng-app="myApp" ng-controller="myCtrl" ng-init="load()">
    <a class="btn btn-default" href="/"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
    <form class="form-horizontal" id="loginform" name="loginform">
        <fieldset>
            <!-- Form Name -->
            <legend>Create Post</legend>
            {{ csrf_field() }}
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="email">Title *</label>
                <div class="col-md-4">
                    <input ng-model="title" id="title" name="text" type="text" class="form-control input-md" required="">
                </div>
                <div class="col-md-4">
                    <span class="title-error validations"></span>
                </div>
            </div>

            <!-- text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Short Description *</label>
                <div class="col-md-4">
                    <input ng-model="short_description" id="short_description" name="short_description" type="text" placeholder="" class="form-control input-md">
                </div>
                <div class="col-md-4">
                    <span class="short-description-error validations"></span>
                </div>
            </div>

            <!-- text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="password">Description *</label>
                <div class="col-md-4">
                    <input ng-model="description" id="description" name="description" type="text" class="form-control input-md">
                </div>
                <div class="col-md-4">
                    <span class="description-error validations"></span>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="createBtn"></label>
                <div class="col-md-4">
                    <a class="btn btn-primary" ng-click="createPost()">Create</a>
                </div>
            </div>

        </fieldset>
    </form>
</div>
<!-- Modal -->

<script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
	var token = localStorage.getItem('access_token');
    var csrf_token = document.getElementsByName('_token')[0].value;

    $scope.load = function() {
		if(token==null){
            window.location = "/user/login";
        }
    }

	$scope.createPost = function(){
        if(this.title!=null && this.short_description!=null && this.description!=null){
            $http({
                method : "POST",
                url : "/api/posts/create",
                data : {
                    title : this.title,
                    short_description : this.short_description,
                    description : this.description,
                },
                headers: {
                    'X-CSRF-TOKEN': csrf_token,
                    'Authorization' : "Bearer "+token,
                    'Accept' : 'application/json'
                }
            })
            .then(function(response) {
                if(response.data.code===200){
                    window.location = "/";
                }
            },function(error){
                $(".title-error").html("Error creating your post.");
            });
        }else{
            // var error = [];
            if(this.title==null){
                // error.push("Title cannot be empty");
                $(".title-error").html("Title cannot be empty");
            }
            if(this.short_description==null){
                // error.push("Short Description cannot be empty");
                $(".short-description-error").html("Short Description cannot be empty");
            }
            if(this.description==null){
                // error.push("Description cannot be empty");
                $(".description-error").html("Description cannot be empty");
            }
        }
	}
});

</script>
@endsection

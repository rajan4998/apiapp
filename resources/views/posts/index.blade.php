@extends('layouts/master')
@section('content')
<style>
.btn-danger:active, .btn-danger {
    background-color: #c9302c;
}
.top-row-btn{
	margin-bottom: 20px;
}
body{
	background-color: #fff;
}
.del-modal-txt{
	margin-left: 15px;
}
</style>
<div class="container" ng-app="myApp" ng-controller="myCtrl" ng-init="load()">
	<div class="top-row-btn">
		<a href="/posts/create" class="btn btn-primary">Create Posts</a>
		<a href="/user/import" class="btn btn-primary">Bulk Upload</a>
	</div>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>Description</th>
				<th>Short Description</th>
			</tr>
		</thead>
		<tbody id="postsTable">
			<tr ng-repeat="item in posts" class="post_@{{item.id}}">
				<td class="post_title_@{{item.id}}">@{{item.title}}</td>
				<td class="post_short_description_@{{item.id}}">@{{item.short_description}}</td>
				<td class="post_description_@{{item.id}}">@{{item.description}}</td>
				<td>
					<a class="btn btn-default edit-post" data-id="@{{item.id}}" data-toggle="modal" data-target="#editModal" ng-click="getPostData()"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
					<a class="btn btn-danger edit-post" data-id="@{{item.id}}" data-toggle="modal" data-target="#deleteModal" ng-click="getPostData()"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
				</td>
			</tr>
		</tbody>
	</table>

	<div id="editModal" class="modal fade" role="dialog" >
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit : @{{staticTitle}}</h4>
				</div>
				<div class="modal-body">
					<form>
						{{ csrf_field() }}
						<input type="hidden" ng-model="post_id" value="@{{post_id}}"/>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label class="control-label pull-right" for="editTitle">Title *</label>
								</div>
								<div class="col-md-7">
									<input ng-model="editTitle" id="editTitle" name="editTitle" value="@{{editTitle}}" type="text" class="form-control input-md" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label class="control-label pull-right" for="editShortDescription">Short Description *</label>
								</div>
								<div class="col-md-7">
									<input ng-model="editShortDescription" id="editShortDescription" name="editShortDescription" value="@{{editShortDescription}}" type="text" class="form-control input-md" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-4">
									<label class="control-label pull-right" for="editDescription">Description *</label>
								</div>
								<div class="col-md-7">
									<input ng-model="editDescription" id="editDescription" name="editDescription" value="@{{editDescription}}" type="text" class="form-control input-md" required>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<a class="btn btn-primary" ng-click="updatePost()" data-dismiss="modal">Update</a>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div id="deleteModal" class="modal fade" role="dialog" >
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Delete : @{{staticTitle}}</h4>
				</div>
				<div class="modal-body">
					<form>
						<input type="hidden" ng-model="post_id" value="@{{post_id}}"/>
						<div class="row">
							<p class="del-modal-txt">Are you sure you want to delete?
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<a class="btn btn-danger" ng-click="deletePost()" data-dismiss="modal">Delete</a>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->

	<script>
	var app = angular.module('myApp', []);
	app.controller('myCtrl', function($scope, $http) {
		var token = localStorage.getItem('access_token');
        var csrf_token = document.getElementsByName('_token')[0].value;

		$scope.load = function() {
			if(token==null){
				window.location = "/user/login";
			}
		}

		$http({
			method : "GET",
			url : "/api/posts",
			headers: {
				'Authorization' : "Bearer "+token
			},
		})
		.then(function(response) {
			$scope.posts = response.data.posts;
		});

        $scope.getPostData = function(){
            $scope.post_id = this.item.id;
            $scope.staticTitle = this.item.title;
            $scope.editTitle = this.item.title;
            $scope.editShortDescription = this.item.short_description;
            $scope.editDescription = this.item.description;
        };

		$scope.updatePost = function(){
			if(this.editTitle!="" && this.editShortDescription!="" && this.editDescription!=""){
				$http({
					method : "POST",
					url : "/api/posts/edit",
					data : {
						post_id :this.post_id,
						title : this.editTitle,
						short_description : this.editShortDescription,
						description : this.editDescription,
					},
					headers: {
						'X-CSRF-TOKEN': csrf_token,
						'Authorization' : "Bearer "+token,
						'Accept' : 'application/json'
					}
				})
				.then(function(response) {
					if(response.data.code===200){
                        var post = response.data.post;
                        for(var i=0;i < $scope.posts.length;i++){
                            if(post.id==$scope.posts[i].id){
                                $scope.posts[i].title= post.title;
                                $scope.posts[i].short_description = post.short_description;
                                $scope.posts[i].description = post.description;
                                break;
                            }
                        }
                    }
				});
			}
		}

		$scope.deletePost = function(){
			$http({
				method : "POST",
				url : "/api/posts/delete",
				data : {
					post_id :this.post_id,
				},
				headers: {
					'X-CSRF-TOKEN': csrf_token,
					'Authorization' : "Bearer "+token,
					'Accept' : 'application/json'
				}
			})
			.then(function(response) {
                if(response.data.code===200){
                    var post = response.data.post;
                    angular.element(document.querySelector('.post_'+ post.id)).remove();
                }
			});
		}
	});
	</script>
	@endsection
